# docker-compose collection

My personnal docker-compose collection

# Update all docker-compose.yml in one command.

```
#!/bin/bash
FILES="/path/to/dockerfiles/folder/**/*"
for f in $FILES
do
  echo "$f"
  docker-compose -f "$f" pull; docker-compose -f "$f" up -d --remove-orphans
done
```

This script is assuming that every dockerfiles are each in a separate folder